// 3. Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)

let num = 2;
const getCube = num ** 3;

// console.log(getCube);


// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…

console.log(`The cube of ${num} is ${getCube}`);


// 5. Create a variable address with a value of an array containing details of an address.

const fullAddress = [258, "Washington Ave", "NW, California", 90011];


// 6. Destructure the array and print out a message with the full address using Template Literals.

const [houseNumber, city, state, zipCode] = fullAddress;

console.log(`I live at ${houseNumber} ${city} ${state} ${zipCode}`);


// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.

const animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: 1075,
	length: "20 ft 3 in"
}

/*8. Destructure the object and print out a message with the details of the animal using Template Literals.*/

const {name, type, weight, length} = animal;

console.log(`${name} was a ${type}. He weighted at ${weight} kgs with a measurement of ${length}.`);


// 9. Create an array of numbers.
const numbersArray = [1, 2, 3, 4, 5];


// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

numbersArray.forEach((number) => {
	console.log(`${number}`);
})

// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.


const sum = numbersArray.reduce((x, y) => x + y);
console.log(sum);


// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed= breed;
	}
}


// 13. Create/instantiate a new object from the class Dog and console log the object.
const myDog = new Dog();

myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";

console.log(myDog);
